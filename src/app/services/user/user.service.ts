import { Injectable } from '@angular/core';
import { ApiData } from '../api.data';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/user.model';
import { UserListResponse } from 'src/app/models/response/user-list-response.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  c = 25;

  p = 2;

  apiUrl = ApiData.url;

  userList: Array<User> = [];

  // build url
  url = `${this.apiUrl}?page=${this.p}&results=${this.c}`;

  public setFilteredItems(searchTerm) {
    return this.userList.filter(item => {
      return (
        item.name.first.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      );
    });
  }

  loadUsersFromServer(callback?: () => void) {
    this.getUserList()
      .then((data: UserListResponse) => {
        data.results.forEach(usr => {
          this.userList.push(usr);
        });
        if (callback) {
          callback();
        }
      })
      .catch(err => {
        console.error(err);
      });
  }

  getUserList() {
    return new Promise((resolve, reject) => {
      this.http.get(this.url).subscribe(
        data => {
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  constructor(public http: HttpClient) {}
}
