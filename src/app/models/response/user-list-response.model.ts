import { User } from "../user.model";
import { Info } from "../info.models";

export interface UserListResponse {
  results: Array<User>;
  info: Info;
}
