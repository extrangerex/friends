import { User } from '../../models/user.model';
import { Component, OnInit, Input } from '@angular/core';
import '../../prototypes/capitalize.prototype';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})

export class UserCardComponent implements OnInit {

  @Input() user: User;

  constructor() {
  }

  ngOnInit() {

  }

}
