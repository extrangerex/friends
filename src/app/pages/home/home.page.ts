import { Component } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { ModalController } from '@ionic/angular';
import { ProfileModalPage } from '../../modals/profile-modal/profile-modal.page';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  count = 0;
  searchTerm = '';
  searchResult: any = [];
  timeOut = 2500;

  // update searchResults when users are updated
  notifyDataset = () => {
    this.setFilterItems();
  }

  async openProfile(user: User) {
    const modal: HTMLIonModalElement = await this.modalCtrl.create({
      component: ProfileModalPage,
      componentProps: {
        user
      }
    });

    await modal.present();
  }

  PulldownRefresh(event) {
    setTimeout(() => {
      this.userService.loadUsersFromServer(this.notifyDataset);
      event.target.complete();
    }, this.timeOut);
  }

  public setFilterItems() {
    this.searchResult = this.userService.setFilteredItems(this.searchTerm);
  }

  downloadMoreFriends(event) {
    // console.log(`more data`);
    setTimeout(() => {
      this.userService.p++;
      this.userService.loadUsersFromServer(this.notifyDataset);
      event.target.complete();
    }, this.timeOut);
  }

  constructor(public userService: UserService, private modalCtrl: ModalController) {
    this.userService.loadUsersFromServer(this.notifyDataset);
  }
}
