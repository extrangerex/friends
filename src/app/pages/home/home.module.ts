import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UserCardComponent } from '../../components/user-card/user-card.component';
import { HomePage } from './home.page';
import { ProfileModalPage } from '../../modals/profile-modal/profile-modal.page';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@NgModule({
  entryComponents: [ProfileModalPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
  ],
  declarations: [HomePage , UserCardComponent, ProfileModalPage],
  providers: [
    CallNumber,
    EmailComposer
  ]
})
export class HomePageModule {}
