interface String {
  toCapitalize: () => string;
}

String.prototype.toCapitalize = function() {
  return this[0].toUpperCase() + this.substr(1).toLowerCase();
};
