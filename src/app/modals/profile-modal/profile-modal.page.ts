import { Component, OnInit, Input } from "@angular/core";
import { User } from "../../models/user.model";
import { NavParams, ModalController } from "@ionic/angular";
import "../../prototypes/capitalize.prototype";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { EmailComposer } from "@ionic-native/email-composer/ngx";

@Component({
  selector: "app-profile-modal",
  templateUrl: "./profile-modal.page.html",
  styleUrls: ["./profile-modal.page.scss"]
})
export class ProfileModalPage {
  user: User;

  constructor(
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private callNumber: CallNumber,
    private emailCpr: EmailComposer
  ) {}

  ionViewWillEnter() {
    this.user = this.navParams.get("user");
  }

  mail() {
    const email = {
      to: this.user.email
    };
    this.emailCpr.open(email);
  }

  call() {
    this.callNumber
      .callNumber(this.user.cell, true)
      .catch(err => console.log("Error launching dialer", err));
  }

  async dismiss() {
    await this.modalCtrl.dismiss();
  }
}
