# friends

Technical test for **apply to a job** as Ionic developer
_remember_ to run `npm install` before testing.

## design explanation

We have decided for the orange as the primary color based on
that to get orange we need to mix red and yellow.

Red for herself can transmit love, passion , power. Also
yellow can transmit hospitality, calm and timelessness. Knowing
what these colors mean we can conclude that the **orange** it's a
color that transmit power , creativity , love... but also calm,
happiness in conclusion `a happy place but also safe`

SELECTED COLOR: **PANTONE 16-1358 TCX**
_the perfect balance between red and yellow_

### User Interface

For interactive prototyping you can visit [here](https://xd.adobe.com/view/318e4522-30ac-40b7-5e3e-4fbae1636961-47bf/)

# Dependencies

- angular
- ionic
- fontAwesome
- HttpClientModule
- CallDialer
- EmailComposer

# Project Structure

- app
  - modals
  - models
  - pages
  - prototypes
  - services
  - values
  - components
